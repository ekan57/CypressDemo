describe('On CAGs web', function () {
    before(() => {
        //Do nothing
    })

    after(() => {
        cy.clearCookies()
    })

    beforeEach(() => {
        cy.visit('https://www.cag.se/')
            .get('#cookie-agree').click()

    })


    it('Find Conctor', function () {
        cy.get('.eut-icon-search').click()
            .get('#eut-main-menu form input').type('Contactor')
            .get('.eut-icon-search').click()
            .get('div.wpb_column.eut-column-1 div.eut-isotope-container article div div div a h4').first().should('contain', 'C.A.G Contactor')

        //.get('#eut-main-menu form input').then(($form) => {
        //    console.log('form is:', $form)
        //    cy.pause()
        //$form.type('{enter}')
        //    .intercept('GET', 'https://www.cag.se/?s=Contactor')
        //    //cy.wait(5000)
        //    .get('div.wpb_column.eut-column-1 div.eut-isotope-container article div div div a h4').first().should('contain', 'C.A.G Contactor')
        //})
    })

    it('Find Arete', function () {
        cy.get('.eut-icon-search').click()
            .get('#eut-main-menu form input').type('Arete').type('{enter}')
            .get('div.wpb_column.eut-column-1 div.eut-isotope-container article div div div a h4').first().should('contain', 'C.A.G Arete')

    })
})


