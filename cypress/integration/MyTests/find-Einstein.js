describe('Finding Einstein', function () {
	before(() => {
		//Do nothing
	})
	
	after(()=>{
		//cy.clearCookies()
	})
	
	it('on duckduckgo', function() {
		cy.visit('https://duckduckgo.com/')
		cy.get('#search_form_input_homepage').type('Einstein').type('{enter}')
		cy.get('a.result__a').first().should('contain', 'Albert')
	})
})